## Introduction

We would like you to design a user interface for a small survey. The focus should be to show a simple interface where the end-user easily can understand the questions and fill in answers.

## Requirements

- Redesign the user interface for this small survey; https://opee.pl/t/b4513751-3296-4846-a1c8-fda5f6e27db6
- One additional matrix question should be added as a last question on the survey
- The interface should be optimized for use on mobile devices
- Write a description of your work and reflections on user experience, style and design choices including possibilities for future expansions 


## Details

The design should be delivered in;

1. One PDF file

2. One File/share from preferred design tool (Figma, Sketch, etc.) 

3. Optional; Implemented as a prototype in HTML/CSS 

Details on matrix question type can be found here; https://bit.ly/3bUBGdW
